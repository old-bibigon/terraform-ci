module "hello_world_app" {

  source = "../../modules/services/hello-world-app"

  server_text = var.server_text

  environment            = var.environment
  cluster_name       = var.cluster_name
#  db_remote_state_bucket = var.db_remote_state_bucket
#  db_remote_state_key    = var.db_remote_state_key

  db_address = module.mysql.address
  db_port = module.mysql.port

  instance_type      = "t2.micro"
  min_size           = 2
  max_size           = 2
  enable_autoscaling = false
}
