resource "aws_launch_configuration" "example" {
    image_id = "ami-0c55b159cbfafe1f0"
    instance_type = var.instance_type
    security_groups = [aws_security_group.instance.id]

    user_data = var.user_data

#    tags = {
#        Name = "terraform-example"
#    }
    # Требуется при использовании группы автомасштабирования
    # в конфигурации запуска.
    # https://www.terraform.io/docs/providers/aws/r/launch_configuration.html
    lifecycle {
        create_before_destroy = true
    }
}


resource "aws_security_group" "instance" {
    name = "${var.cluster_name}-instance"
    ingress {
        from_port = var.server_port
        to_port = var.server_port
        protocol = "tcp"
        cidr_blocks = local.all_ips
    }
}


resource "aws_autoscaling_group" "example" {
    # Напрямую зависит от имени конфигурации запуска, поэтому вместе
    # с ним всегда нужно заменять и эту группу ASG
    name = "${var.cluster_name}-${aws_launch_configuration.example.name}"

    launch_configuration = aws_launch_configuration.example.name
    vpc_zone_identifier = var.subnet_ids

    # Настраиваем интеграцию с балансировщиком нагрузки
    target_group_arns = var.target_group_arns
    health_check_type = var.health_check_type

    min_size = var.min_size
    max_size = var.max_size

    min_elb_capacity = var.min_size

    tag {
        key = "Name"
        value = var.cluster_name
        propagate_at_launch = true
    }
}


