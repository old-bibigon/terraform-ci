variable "cluster_name" {
    description = "The name to use for all the cluster resources"
    type = string
}

variable "min_size" {
    description = "The minimum number of EC2 Instances in the ASG"
    type = number
}

variable "max_size" {
    description = "The maximum number of EC2 Instances in the ASG"
    type = number
}


variable "environment" {
    description = "The name of the environment we're deploying to"
    type = string
}


variable "enable_autoscaling" {
  description = "If set to true, enable auto scaling"
  type        = bool
}


variable "ami" {
  description = "The AMI to run in the cluster"
  type        = string
  default     = "ami-0c55b159cbfafe1f0"
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
  default     = "t2.micro"
}

variable "server_text" {
  description = "The text the web server should return"
  default     = "Hello, World"
  type        = string
}

variable "server_port" {
    description = "The port the server will use for HTTP requests"
    type = number
    default = 8080
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG"
  type        = map(string)
  default     = {}
}

variable "db_port" {
    description = "The port of db server"
    type = number
    default = 8080
}

variable "db_address" {
    description = "The port the server will use for HTTP requests"
    type = string
}
