#!/bin/bash
cat > index.html <<EOF
<hl>Hello, World</hl>
<p>DB address: ${db_address}</p>
<p>DB port: ${db_port}</p>
EOF

nohup busybox httpd -f -p ${server_port} &
